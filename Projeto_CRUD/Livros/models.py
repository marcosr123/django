from django.db import models
from django.urls import reverse

# Create your models here.

class Livro(models.Model):
    NomeLivro = models.CharField(max_length=200)
    PagLivro = models.IntegerField()

    def __str__(self):
        return self.NomeLivro

    def get_absolute_url(self):
        return reverse('livro_list', kwargs={'pk':self.pk})
