from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm

from Livros.models import Livro

# Create your views here.

class LivroForma(ModelForm):
    class Meta:
        model = Livro
        fields = ['NomeLivro', 'PagLivro']

def livro_list(request, template_name='Livros/livro_list.html'):
    livro = Livro.objects.all()
    data = {}
    data['object_list'] = livro
    return render(request, template_name, data)

def livro_view(request, pk, template_name='Livros/livro_detalhes.html'):
    livro = get_object_or_404(Livro, pk=pk)
    return render(request,template_name,{'object':Livro})

def livro_criar(request, template_name='Livros/livro_form.html'):
    form = LivroForma(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(livro_list)
    return render(request, template_name, {'form':form})

def livro_edita(request, pk, template_name='Livros/livro_form.html'):
    livro = get_object_or_404(Livro, pk=pk)
    form = LivroForma(request.POST or None, intance=Livro)
    if form.is_valid():
        form.save()
        return redirect(livro_list)
    return render(request, template_name, {'form':form})

def livro_deleta(request, pk, template_name='Livros/livro_confirma_deleta.html'):
    book= get_object_or_404(Livro, pk=pk)
    if request.method=='POST':
        book.delete()
        return redirect(livro_list)
    return render(request, template_name, {'object':book})
