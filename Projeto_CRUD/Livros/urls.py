from django.urls import path
from . import views

urlpatterns = [
    path('', views.livro_list, name='lista_livro'),
    path('view/<int:pk>', views.livro_view, name='livro_view'),
    path('new/', views.livro_criar, name='livro_novo'),
    path('edit/<int:pk>', views.livro_edita, name='livro_edita'),
    path('delete/<int:pk>', views.livro_deleta, name='livro_deleta'),
]
