from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('Livros/', include('Livros.urls')),
    path('admin/', admin.site.urls),
]
